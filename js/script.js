//HTML
const inputTable = document.querySelector('input[type="text"]');
const keysTable = document.getElementsByClassName('keys')[0];
const divMRC = document.getElementsByClassName('mrc')[0];

sessionStorage.clear();



//Helpers
const updateFirstNumber = () => {

    let newNumber = sessionStorage.getItem("First_number");
    if (newNumber === null) {
        sessionStorage.setItem("First_number", event.target.value);
        inputTable.value += event.target.value;
    } else {
        newNumber += event.target.value;
        sessionStorage.setItem("First_number", newNumber);
        inputTable.value += event.target.value;
    }
};

const updateSecondNumber = () => {

    let newNumber = sessionStorage.getItem("Second_number");
    if (newNumber === null) {
        sessionStorage.setItem("Second_number", event.target.value);
        inputTable.value += event.target.value;
    } else {
        newNumber += event.target.value;
        sessionStorage.setItem("Second_number", newNumber);
        inputTable.value += event.target.value;
    }
};

const setNumberToSessionStorage = () => {
    if (sessionStorage.getItem("operation") === null) {
        updateFirstNumber();
    } else {
        updateSecondNumber();
    }
};

const checkOperation = () => {
    let operation = sessionStorage.getItem("operation");
    if (operation !== null && sessionStorage.getItem("Second_number") === "") {
        inputTable.value = '0';
    } else if (sessionStorage.getItem('Operation_click') === "true") {
        sessionStorage.removeItem("Second_number");
        inputTable.value = '';
        sessionStorage.setItem('Operation_click', "false");
    }
};

const calcNumbers = () => {
    if(sessionStorage.getItem("First_number") !== null && sessionStorage.getItem("Second_number") !== null && sessionStorage.getItem("operation") !== null) {
        let firstNumber = sessionStorage.getItem("First_number");
        let secondNumber = sessionStorage.getItem("Second_number");
        let operation = sessionStorage.getItem("operation");
        let calc = 0;
        switch(operation) {
            case '+':
                calc = +(Number(firstNumber) + Number(secondNumber)).toFixed(16);
                break;
            case '-':
                calc = +(Number(firstNumber) - Number(secondNumber)).toFixed(16);
                break;
            case '*':
                calc = +(Number(firstNumber) * Number(secondNumber)).toFixed(16);
                break;
            case '/':
                calc = +(Number(firstNumber) / Number(secondNumber)).toFixed(16);
                break;
        }
        inputTable.value = calc;
        sessionStorage.setItem("First_number", inputTable.value);
    }
};

const clearNumbers = () => {
    sessionStorage.removeItem("First_number");
    sessionStorage.removeItem("Second_number");
};

const clearOperation = () => {
    sessionStorage.removeItem("operation");
    sessionStorage.removeItem("Operation_click");
};

const plusSaveNumber = () => {
    let newNumber = sessionStorage.getItem("Save_number");
    if (newNumber === null) {
        sessionStorage.setItem("Save_number", inputTable.value);
    } else {
        newNumber = +(Number(newNumber) + Number(inputTable.value)).toFixed(16);
        sessionStorage.setItem("Save_number", newNumber);
    }
    divMRC.style.display = "block";
};

const minusSaveNumber = () => {
    let newNumber = sessionStorage.getItem("Save_number");
    if (newNumber === null) {
        newNumber = 0 - Number(inputTable.value);
        sessionStorage.setItem("Save_number", newNumber);
    } else {
        newNumber = +(Number(newNumber) - Number(inputTable.value)).toFixed(16);
        sessionStorage.setItem("Save_number", newNumber);
    }
    divMRC.style.display = "block";
};


//Events
keysTable.addEventListener('click', function(event) {
    if (event.target.classList.contains('button')){
        if(!isNaN(Number(event.target.value)) && event.target.value !== "0") {

            checkOperation();
            sessionStorage.setItem("Operation_click", "false");
            if (sessionStorage.getItem("First_number") === null || sessionStorage.getItem("Equal_Showed") === "true") {
                inputTable.value = '';
                sessionStorage.removeItem("Equal_Showed");
                clearNumbers();
                clearOperation();
            }
            if (inputTable.value === '0') {
                inputTable.value = '';
            }

            setNumberToSessionStorage();
            sessionStorage.setItem("Save_number_Showed", "false");

        } else if (event.target.value === "0" ) {

            checkOperation();
            sessionStorage.setItem("Operation_click", "false");
            if (sessionStorage.getItem("First_number") === null || sessionStorage.getItem("Equal_Showed") === "true") {
                inputTable.value = '0';
                clearNumbers();
                clearOperation();
                sessionStorage.removeItem("Equal_Showed");
                sessionStorage.setItem("First_number", "0");
                return false;
            } else if (sessionStorage.getItem("First_number") === "0") {
                return false;
            } else {
                setNumberToSessionStorage();
            }

            sessionStorage.setItem("Save_number_Showed", "false");

        } else if (event.target.value === "." ) {

            checkOperation();
            sessionStorage.setItem("Operation_click", "false");
            if (inputTable.value === '') {
                inputTable.value = '0';
            }

            if (sessionStorage.getItem("Equal_Showed") === "true") {
                inputTable.value = '0';
                clearNumbers();
                clearOperation();
                sessionStorage.removeItem("Equal_Showed");
            }

            let checkPoint = [...inputTable.value].filter(item => item === ".");

            if (checkPoint.length === 0) {
                setNumberToSessionStorage();
            }

            sessionStorage.setItem("Save_number_Showed", "false");

        } else if (event.target.value === "*" || event.target.value === "/" || event.target.value === "+" || event.target.value === "-") {

            sessionStorage.setItem("Operation_click", "true");
            if(sessionStorage.getItem("Equal_Showed") === "true") {
                // sessionStorage.setItem("Second_number", '');
                sessionStorage.removeItem("Equal_Showed");
            } else {
                calcNumbers();
            }

            if (sessionStorage.getItem("Second_number") !== null && sessionStorage.getItem("operation") !== null) {
                sessionStorage.setItem("operation", event.target.value);
                return false;
            } else if (sessionStorage.getItem("First_number") === null) {
                sessionStorage.setItem("First_number", "0");
                sessionStorage.setItem("operation", event.target.value);
                sessionStorage.setItem("Second_number", '');
            } else {
                sessionStorage.setItem("operation", event.target.value);
                sessionStorage.setItem("Second_number", '');
            }

            sessionStorage.setItem("Save_number_Showed", "false");

        } else if (event.target.value === "C" ) {
            clearNumbers();
            clearOperation();
            inputTable.value = '0';
            sessionStorage.setItem("Save_number_Showed", "false");
            sessionStorage.removeItem("Equal_Showed");

        } else if (event.target.value === "=" ) {
            sessionStorage.setItem("Operation_click", "false");
            if (sessionStorage.getItem("First_number") !== null && sessionStorage.getItem("operation") !== null && sessionStorage.getItem("Second_number") !== null) {
                calcNumbers();
                sessionStorage.removeItem("First_number");
                sessionStorage.setItem("Equal_Showed", "true");
                if (sessionStorage.getItem("Equal_Showed") === "true") {
                    sessionStorage.setItem("First_number", inputTable.value);
                }
            } else {
                return false;
            }

            sessionStorage.setItem("Save_number_Showed", "false");

        } else if (event.target.value === "m+" ) {
            plusSaveNumber();
        } else if (event.target.value === "m-" ) {
            minusSaveNumber();
        } else if (event.target.value === "mrc" ) {

            if(sessionStorage.getItem("Save_number") === null) {
                return false;
            } else {
                if (sessionStorage.getItem("Equal_Showed") === "true") {
                    clearNumbers();
                    clearOperation();
                    sessionStorage.removeItem("Equal_Showed");
                }

                inputTable.value = sessionStorage.getItem("Save_number");

                if (sessionStorage.getItem("Second_number") === null) {
                    sessionStorage.setItem("First_number", inputTable.value);
                } else {
                    sessionStorage.setItem("Second_number", inputTable.value);
                }

                if (sessionStorage.getItem("Save_number_Showed") === "true") {
                    sessionStorage.removeItem("Save_number");
                    sessionStorage.removeItem("Save_number_Showed");
                    divMRC.style.display = "none";
                }

                sessionStorage.setItem("Save_number_Showed", "true");
            }
        }
    }
});

