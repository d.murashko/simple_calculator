**`Calculator`**

This is a device that performs arithmetic operations on numbers. 
It can do addition, subtraction, multiplication, and division and something else.

**`What was used`**

- HTML
- CSS
- Native JavaScript
- Session Storage

**`See application`**

You have to clone this project to your computer and run `index.html`

###### or

Click this page: https://d.murashko.gitlab.io/simple_calculator or https://di-m-calculator.netlify.app/